/*
  Constantes encabezados, de uso general
*/

const CODIGO = 'Código';
const NOMBRE = 'Nombre';
const BATERIA = 'Batería';
const MEMORIA = 'Memoria';
const ULTIMO_DATO = 'Último dato';
const TPO_OFFLINE = 'Tiempo Offline';
const DOP = 'Dilution of Precision';

export { CODIGO, NOMBRE, BATERIA, MEMORIA, ULTIMO_DATO, TPO_OFFLINE, DOP };
