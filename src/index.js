

import { CODIGO, NOMBRE, BATERIA, MEMORIA, ULTIMO_DATO, TPO_OFFLINE, DOP } from './names';
import {
  gauge_tpl,
  fuel_gauge_tpl,
  time_tpl,
  load_template,
  give_values,
  tpl_gauge_opts,
  tpl_time_opts,
  twocircles_tpl,
  tpl_dop_opts,
} from './templates';
import {
  get_last_element,
  findPosition,
  add_row2table,
  print,
  complete_table,
  update_gauge_value,
  update_fuel_gauge_value,
  update_table,
  deltatime,
  update_field,
} from './table_manage';
import { gaugeCharts, fuelGaugeCharts, twoCirclesCharts } from './charts';

function createStationList(stationList) {
  if (stationList.length == 0) {
    stationList = [
      { type: 'rtx', pos: { lat: -25.36, lon: -71.361 }, station: 'valn' },
      { type: 'no_rtx', pos: { lat: -26.36, lon: -71.561 }, station: 'trpd' },
      { type: 'rtx', pos: { lat: -28.36, lon: -71.361 },  station: 'qtay' },
      { type: 'rtx', pos: { lat: -30.36, lon: -70.51 },  station: 'ccsn' },
      { type: 'no_csn', pos: { lat: -41.36, lon: -70.61 },  station: 'atjn' },
      { type: 'no_rtx', pos: { lat: -38.36, lon: -70.361 },  station: 'bing' },
    ];
    return stationList;
  } else {
    return stationList;
  }
}


function compare(a,b){
    if (a.pos.lat<b.pos.lat){
        return 1;}
    if (a.pos.lat>b.pos.lat){
        return -1;
    }
    return 0;
}

export function getStations(){
    let jsonData = document.getElementById('station_list').textContent;
    let stationList = [];
    if (jsonData.trim().length > 0) {
      stationList = JSON.parse(jsonData);
    }
    stationList = createStationList(stationList);
    stationList.sort(compare);
    return stationList;
}



let key_name_json = {
  [CODIGO]: ['station'],
  [NOMBRE]: ['name'],
  [BATERIA]: ['data', 'BATT_MEM', 'BATT_CAPACITY'],
  [MEMORIA]: ['data', 'BATT_MEM', 'REMAINING_MEM'],
  [ULTIMO_DATO]: ['data', 'BATT_MEM', 'TIMESTAMP'],
  [TPO_OFFLINE]: ['to'],
  [DOP]: ['data', 'DOP'],
};


/*
Posiciones de cada columna en la tabla
*/
let column_map = {
  [CODIGO]: 0,
  [NOMBRE]: 1,
  [BATERIA]: 2,
  [MEMORIA]: 3,
  [ULTIMO_DATO]: 4,
  [TPO_OFFLINE]: 5,
  [DOP]: 6,
};

/*
funciones de operacion sobre valores
*/
function simple(value) {
  return value;
}

function porcentaje_mem(value, base) {
  return value;
}

function ts2datetime(value) {
  let datetime = new Date(value * 1000);
  return datetime;
}

function timeOnline(value) {
  return value;
}
/**/
let table_map = {};

export function getOpts(){
    const start = 20;
    const range = (90-start)*2;
    const end=start+range;

    let other_opts = {
        row_click:{'callback':console.log, 'args':["test"]},
      tpl_path: {
        gauge: gauge_tpl,
        fuel_gauge: fuel_gauge_tpl,
        datetime: time_tpl,
        dop: twocircles_tpl,
      },
      size: { w: 80, h: 80 },
      with_tpl: {
        [BATERIA]: { tipo: 'gauge', op: simple, op_args: [] },
        [MEMORIA]: { tipo: 'gauge', op: porcentaje_mem, op_args: [] },
        [ULTIMO_DATO]: { tipo: 'datetime', op: ts2datetime },
        op_args: [],
        [TPO_OFFLINE]: { tipo: 'fuel_gauge', op: timeOnline, op_args: [] },
        [DOP]: { tipo: 'dop', op: simple, op_args: [] },
      },
      charts: { liquid_gauge: gaugeCharts, fuel_gauge: fuelGaugeCharts, dop: twoCirclesCharts },
      chart_opts: {
        [BATERIA]: {
          circleThickness: 0.15,
          textVertPosition: 0.8,
          waveAnimate: true,
          textSize: 0.75,
          waveCount: 3,
          waveAnimateTime: 1000,
          textSize: 0.8,
        },
        [MEMORIA]: {
          circleThickness: 0.15,
          textVertPosition: 0.8,
          waveAnimate: true,
          textSize: 0.8,
          minValue: 0,
          maxValue: 4000,
          waveAnimateTime: 1000,
          waveCount: 5,
          circleColor: '#F4D03F',
          waveColor: '#F5B041',
          textTextColor: '#229954',
          waveTextColor: '#229954',
        },
          [TPO_OFFLINE]: {
              size: 100,
              minorTicks: 1,
              showPointer: true,
              showValue: true,
              valueFontSize: 12,
              min: start,
              max: start + range,
              up: 0.85,
              low: 0.45,
              margin_radius: 0.04,
              unit: 's',
              redZone: {
                  from: start+2*range/3,
                  to: end  ,
              },
              yellowZone: {
                  from: start+range/3,
                  to: start+2*range/3
              },
              greenZone: {
                  from: start,
                  to:  start+range/3,
              },

          },
        [DOP]: {
          width: 120,
          height: 80,
          viewport: { w: 70, h: 40 },
          separation: 12,
          margin: 10,
        },
      },
    };
    return other_opts;
};

/*
Time checking
  Function W3C Standar WS Client
*/
/*
  Define URL
*/
let kwargs = {
  delta_time: 2,
  port: 8000,
  path: 'ws/status_all/ws_users/',
};

/*
init js script
*/

//let init_time_on = update_field('data_table', ULTIMO_DATO, deltatime, [], table_map, column_map);

/*
setInterval(function() {
  update_field('data_table', ULTIMO_DATO, deltatime, [], table_map, column_map);
}, 1000);*/
//w3c_websocket_client(uri);

export {complete_table,
        table_map,
        key_name_json,
        column_map,
        update_table,
        update_field,
        deltatime,
        ULTIMO_DATO};
