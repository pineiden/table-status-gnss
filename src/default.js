import { complete_table,
         getStations,
         getOpts,
         key_name_json,
         table_map} from './index';


const stationsList = getStations();
console.log('==================');
console.log('==================');
console.log('Station List To Table->', stationsList);
console.log('==================');
console.log('==================');
const otherOpts=getOpts();
console.log("Complete Table", complete_table);
complete_table('data_table', key_name_json, stationsList, table_map, otherOpts);
