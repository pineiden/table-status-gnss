/*
  Template declaration
*/
const gauge_tpl = `
        <div id="item_CODE">
            <div class="value"></div>
            <div class="gauge_status">
                <svg id="chart_CODE" width="WIDTH", height="HEIGHT" onclick=""></svg>
            </div>
        </div>
        `;

const fuel_gauge_tpl = `
        <div id="item_CODE">
            <div class="value"></div>
            <div class="gauge_time_on">
                <svg id="chart_CODE" width="120", height="60" onclick=""></svg>
            </div>
        </div>
        `;

const time_tpl = `
        <div id="item_time_CODE">
            <div class="value"></div>
            <div class="datetime">
            </div>
        </div>`;

const twocircles_tpl = `
        <div id="item_dop_CODE">
            <div class="value"></div>
            <div class="dop">
                <svg id="chart_dop_CODE" width="120", height="80" onclick=""></svg>
            </div>
        </div>`;

/**/

function load_template(tpl) {
  //let tpl_file = fs.readFile(path,'utf-8');
  let templates = {};
  Object.keys(tpl).forEach(tpl_name => {
    switch (tpl_name) {
      case 'gauge':
        templates[tpl_name] = gauge_tpl;
        break;
      case 'fuel_gauge':
        templates[tpl_name] = fuel_gauge_tpl;
        break;
      case 'datetime':
        templates[tpl_name] = time_tpl;
        break;
      case 'dop':
        templates[tpl_name] = twocircles_tpl;
        break;
    }
  });
  return templates;
}

function give_values(tpl_file, opts) {
  let copy_html = tpl_file;
  Object.entries(opts).forEach((item, value) => {
    copy_html = copy_html.replace(new RegExp(item[0], 'g'), item[1]);
  });
  return copy_html;
}

function tpl_gauge_opts(code, width, height) {
  return { CODE: code, WIDTH: width, HEIGHT: height };
}

function tpl_time_opts(code) {
  return { CODE: code };
}

function tpl_dop_opts(code) {
  return { CODE: code };
}

export { gauge_tpl, fuel_gauge_tpl, time_tpl, load_template, give_values, tpl_gauge_opts, tpl_time_opts, tpl_dop_opts };
