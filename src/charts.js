const fillgauge=require('@csn_chile/liquidfillgauge');
const fuelgauge=require('@csn_chile/fuelgauge');
const twoc = require('@csn_chile/twocircleschart');

const { TwoCirclesChart } = twoc.TwoCirclesChart;

function createGauge(chartId, gaugeParams, initialValue) {
  const config = fillgauge.liquidFillGaugeDefaultSettings();
  if (gaugeParams) {
    const gp = gaugeParams;
    Object.keys(config).forEach((key) => {
      if ({}.hasOwnProperty.call(gp, key)) {
        config[key] = gp[key];
      }
    });
  }
    const gauge = fillgauge.loadLiquidFillGauge(chartId, initialValue, config);
    gauge.update(initialValue);
  return gauge;
}

function createFuelGauge(chartId, gaugeParams) {
  const config = {};
  if (gaugeParams) {
    const gp = gaugeParams;
    Object.keys(config).forEach((key) => {
      if ({}.hasOwnProperty.call(gp, key)) {
        config[key] = gp[key];
      }
    });
  }

    const gauge = new fuelgauge.Gauge(`#${chartId}`, gaugeParams);
    gauge.render(50);

  return gauge;
}

// chart: Two Circles (plus Ellipsis)

function createTwoCircles(chartId, dopParams) {
  const twocircles = twoc.TwoCirclesChart(`#${chartId}`, dopParams);
  return twocircles;
}


const gaugeCharts = {
};

const fuelGaugeCharts = {
};

const twoCirclesCharts = {};

function loadChart(code, type, idc, chart, chartMap) {
  /*
      crea un JSON con un id y el objeto
      Carga en chartMap el item

  */
  const itemChart = { id: idc, object_chart: chart };
  if (!chartMap.hasOwnProperty(code)) {
    chartMap[code] = {};
  }
  chartMap[code][type] = itemChart;
}


export {
  createGauge,
  createFuelGauge,
  createTwoCircles,
  loadChart,
  gaugeCharts,
  fuelGaugeCharts,
  twoCirclesCharts,
};
